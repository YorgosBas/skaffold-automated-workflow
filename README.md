An automatic workflow using Skaffold for a PHP file. Each time the file is saved with changes, it begins the build process, creates the image, pushes into the registry and caches the deployment

#### Initialise
```yaml
skaffold init
```
Skaffold is looking for the kubectl command
```yaml
sudo snap alias microk8s.kubectl kubectl
```
Run the skaffold command
```yaml
skaffold dev --default-repo=localhost:32000
```
Acess the app in the browser from the pod
```yaml
microk8s.kubectl port-forward pod/skaffold-pod 8080:4000
```